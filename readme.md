# Bachelor thesis [WIP]

Bachelor thesis around [TravianBotSharp](https://github.com/Erol444/TravianBotSharp). The thesis is written in Markdown and will be converted to PDF when finished.

- Title: `Avtomatizacija procesov na spletu s Selenium ogrodjem`
- Professor: prof. dr. Iztok Fajfar

### Table of contents

1. [Introduction](introduction.md)
2. [Web Process Automation](web-process-automation.md)
3. [Avoiding Detection](avoiding-detection.md)
4. [TravianBotSharp Architecture](architecture.md)
5. [Competition, monetization, and potential improvements](monetization-and-improvements.md)
6. [Community](community.md)
7. [Literature](literature.md)

### TODO

- Translate to Slovene (if required)
- When finished, port over to GDocs for printing & styling
- Add Abstract, Conclusion, and clean up Literature