# TBS Community

I open-sourced the TravianBotSharp program on GitHub in August 2020 and created a Discord server for communication. The [TravianBotSharp Github repository](https://github.com/Erol444/TravianBotSharp/) has 57 stars and 34 forks in the time of writing, and over 4000 people have joined the Discord server. I have collaborated with developers on this project, mainly an Vietnamese developer called [Vinaghost](https://github.com/vinaghost) who has since took over the project and is actively developing it.

![github repo](./images/github-repo.png)