# Literature

- https://blog.travian.com/2023/06/game-secrets-what-is-map-sql/
- https://www.inactivesearch.it/inactives
- http://travian.kirilloid.ru/
- https://html-agility-pack.net/
- https://traviantactics.com/
- https://github.com/justcoding121/Titanium-Web-Proxy
- https://www.zenrows.com/blog/selenium-avoid-bot-detection
- https://github.com/ultrafunkamsterdam/undetected-chromedriver
- Bot prevention system https://www.akamai.com/
- Bot for Supreme merch buying https://supremebot.io/
- Twitter number of bots https://arxiv.org/pdf/1703.03107.pdf
- Chegg podcase https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5zaW1wbGVjYXN0LmNvbS9NWno2VmNvcw/episode/YWE2NjEwNGItNTNmMS00YjBhLWJhNzAtMDcyY2Y1MmY1MWUx?sa=X&ved=0CAIQuIEEahcKEwjwhpODt8X2AhUAAAAAHQAAAAAQCg
- Akamai's Nike collaboration https://www.bloomberg.com/news/features/2021-10-25/how-bots-are-buying-up-all-this-year-s-hottest-christmas-gifts