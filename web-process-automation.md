
# Web Process Automation

## Programs for Web Process Automation

Programs for automating web processes have existed since the inception of browsers. These programs use various methods to obtain data and communicate with web applications.

Initially, browser extension programs, such as Greasemonkey, which allow users to run arbitrary JavaScript programs on web pages, were quite popular. This allows programs to read HTML code and perform actions such as mouse clicks and text input. However, these programs have limitations, such as not being able to run in the background, not being able to run on multiple tabs, and not being able to run on multiple browsers.

Later, programs like Selenium were developed. Selenium is a framework for automating web browsers. It allows users to write programs that control the browser and perform actions like clicking buttons, entering text, and executing JavaScript code. Selenium can run in the background, on multiple tabs, and on multiple browsers. It can also run the browser in headless mode, which means the browser is not visible to the user, and reduces the amount of resources the browser uses.

## Use of Programs for Web Process Automation

These programs exist because the web application does not have a public interface for a user program (known as an Application Programming Interface or API), through which your program could more easily communicate with the web application. Web applications typically do not have a public API because:
a) They do not want automated programs (bots) on their sites. Example: the real estate web application nepremicnine.net. Since they do not have an API, creating a bot is the only way to automatically get real estate data. Of course, such programs are against their ToC (terms and conditions) because ads are a large part of the revenue for nepremicnine.net, and programs do not view the ads.
b) They are simple static web pages with low traffic, making API development not worthwhile. Example: a simple static website about the current weather.

Programs for automating web processes are broadly divided into two categories:

- Programs that only read and parse important information from the HTML code, known as "scraping bots." Example: automatically retrieving data about all properties from nepremicnine.net.
- Programs that perform actions (mouse clicks, text input) based on parsed information, known as "bots." Example: automatically purchasing products from an online store.

### Examples of Web Process Automation:

- Osman Rashid, co-founder of the multi-billion-dollar company Chegg, mentioned in [a Starting Greatness podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5zaW1wbGVjYXN0LmNvbS9NWno2VmNvcw/episode/YWE2NjEwNGItNTNmMS00YjBhLWJhNzAtMDcyY2Y1MmY1MWUx?sa=X&ved=0CAIQuIEEahcKEwjwhpODt8X2AhUAAAAAHQAAAAAQCg) that they initially manually purchased used books through a computer. Due to the company's rapid growth, they soon had five employees buying books manually for 16 hours a day. One of the engineers created a program over the weekend to automatically buy used books through the browser, which, due to high demand, bought a book every 10 seconds. This helped the company continue growing.
- On social networks like Twitter and Facebook, there are many fake profiles. [Some studies](https://arxiv.org/pdf/1703.03107.pdf) estimate nearly 50 million fake profiles on Twitter. In most cases, these profiles aim to manipulate public opinion on political topics, such as elections, COVID-19 measures, and the dangers of 5G wireless radiation.
- The popular fashion brand Supreme usually sells out its entire stock in less than a minute on its online store. These fashion items (clothes, shoes, bags, etc.) are often bought and resold for several times the original price. People frequently use programs to automate the purchase of products from the Supreme online store, as the program is much faster (and therefore more successful) than a person at purchasing. For example, [SuperBot](https://supremebot.io/) typically buys a product in less than 3 seconds.

## Prevention of automation programs
Bot prevention by itself is a huge market - over $1 billion, and it’s rapidly growing. The main companies in this space are Akamai, Clodlare, and ParimeterX.
These systems use advanced techniques to distinguish between legitimate and malicious traffic. These systems employ machine learning and behavioral analysis to identify and block bots while allowing legitimate users and beneficial bots to pass through.
Bot prevention is often described as a cat-and-mouse game because attackers constantly evolve their bots to evade detection, while security systems continuously update their defenses to counter these new tactics. Attackers develop more sophisticated methods to bypass security measures as soon as one type of bot is blocked. This ongoing cycle of adaptation and counter-adaptation creates a dynamic and challenging environment, requiring constant vigilance and innovation to stay ahead of malicious actors.

### Akamai Technologies
Akamai Technologies is a global content delivery network (CDN) and cloud service provider that speeds up web content delivery by caching it on servers near users. They offer web security solutions like web application firewalls, DDoS protection, and bot management. Akamai also supports edge computing for faster data processing and high-performance media streaming. Their services enhance web performance, security, and user experience.
Akamai’s bot prevention system uses behavioral analysis, machine learning, and device fingerprinting to detect and block malicious bots. It employs rate limiting to prevent excessive requests, presents CAPTCHA challenges to verify human users, and leverages a database of known threats for quick identification. The system provides real-time monitoring and automated responses to keep websites secure and accessible for legitimate users.
In 2021, they released a Bloomberg Business week report where they mentioned they’re the bot prevention technology for one of the largest online retailers; Nike, Adidas, and Walmart. The bot prevention system is very profitable and generates around $200 million annually with a growth rate of 40% per year (see 2. literature).
The sneaker resale market itself is estimated to be around $2 billion and is increasing rapidly.

## Automation of Travian game

The Travian game is a browser-based strategy game where players build and develop villages, train troops, and engage in battles with other players. Travian Legends (version 4) is developed in PHP and submits forms using POST requests. Whenever the player performs an action, the game sends a POST request to the server with the action data. The server then processes the request and sends back a response (in form of a new HTML page) with the updated game state. This is why Selenium with Chrome driver is a good choice for automating the game - it can read the HTML code, click buttons, and input text into text fields.

### Travian Kingdoms

Newer version of Travian called Travian Kingdoms (version 5) uses a different technology stack - by using [Wappalyzer](https://www.wappalyzer.com/), I found out it uses AngularJS (1.4.3) frontend and sockets.io for real-time communication between the frontend and backend. This means that the game is now a single-page application (SPA) and uses WebSockets for real-time communication. This makes it easier to automate, because you don't need Selenium/Chrome browser, but just a WebSocket client which you can automate with a simple HTTP client. You'd just need to reverse engineer the WebSocket messages and send the appropriate messages to the server. This has been done by a user called breuerfelix on GitHub, who created a [Travian Kingdom bot](https://github.com/breuerfelix/king-bot). This bot is written in Python and uses the `requests` library to send HTTP requests and the `websocket-client` library to communicate with the server via WebSockets. Felix mentioned that many players have been using his bot for over a year without any bans.

### TTWars

Because Travian is a popular game and has great revenue, there are many clones of the game. One of the most popular clones is [TTWars](https://ttwars.com). TTWars essentially copied the Travian game and just sped up the game activity (to eg. 10000x). This made it perfect for the development of the TravianBotSharp program, as it allowed me to have shorter iteration cycles between the development and testing of the program. TTWars uses the same technology stack as Travian - PHP and POST requests, so the program was easily adapted to work with TTWars (with minor changes).

### Next Page: [Avoiding Detection](avoiding-detection.md)