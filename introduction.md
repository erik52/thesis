# Introduction

During my studies at the Faculty of Electrical Engineering, I developed a program called TravianBotSharp (TBS). The program automates processes in the strategic online game Travian. The program is written in C# and uses the Selenium framework to control the Google Chrome browser. The program automates all repetitive processes in the game, such as building construction, sending and training troops, sending resources to other settlements, founding new settlements, sending the hero on adventures, etc.

TBS is currently a Windows Forms application and has around 20.000 lines of code. It can only be run on the Windows operating system and requires the Google Chrome browser to function. Through the Chrome browser, Selenium performs the following actions:

- Reading the HTML code of the web page,
- Clicking a button,
- Entering text into a text field,
- Clicking a radio button, and
- Executing JavaScript code in the browser.

The program initially reads the web page, parses important information from the HTML code, and then performs tasks based on the settings.

In this thesis I will describe the development of the program, the architecture of the program, the competition in the Travian bot space, and potential monetization and improvements to the program.

### Next Page: [Web Process Automation](web-process-automation.md)