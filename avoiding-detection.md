
# Avoiding detection

To avoid detection, the program must simulate human behavior as closely as possible. This includes:
- Randomizing the time between actions,
- Randomizing the time the program is active,
- Randomizing the mouse movement,
- Different user agents,
- Different IP addresses,
- Different screen resolutions,
- Different operating systems, etc.

For my specific case (the Travian game), their bot detection system is not very advanced. They rarely use CAPTCHA challenges, and they do not have a system for detecting bots based on mouse movements. From extensive testing, we found out that they mainly detect bots based on the activity from the same IP address, and timing between actions. This means you'd want to have a maximum of 6 hours of activity from the same IP address, and you'd want to have a random time between actions (e.g., 1-3 seconds).

## Proxy servers

Proxy servers (proxies) are used to hide the user's IP address. This helps users to avoid getting detected by websites that track their IP address. All requests from the user's computer go through the proxy server, which then forwards the requests to the target website (Travian). This way, the Travian sees the IP address of the proxy server, not the user's IP address.

![](./images/proxy.png)

Travian has an internal bot detection system that tracks the activity of each user (each IP address). Usually, you will get flagged as suspicious  if you have more than 6-8 hours per day of activity from the same IP address. To avoid this, you can use 3 different proxy servers, and switch between them every 2 hours. This way, you can have up to 18-24 hours of account coverage per day without getting flagged as suspicious.

### SeleniumProxyAuth

While developing the TravianBotSharp program, I encountered a limitation of Selenium - it does not support proxy authentication. This is a problem because most paid proxy servers require basic (username and password) authentication.
To solve this problem, I created a .NET library called SeleniumProxyAuth (https://github.com/Erol444/SeleniumProxyAuth). This program runs a local proxy server that does not require authentication. SeleniumProxyAuth then forwards all requests to the proxy server with authentication. Selenium can then connect to the local proxy server without authentication. This way, you can use a proxy server with authentication in Selenium.
[SeleniumProxyAuth](https://www.nuget.org/packages/SeleniumProxyAuth/) is a simple .NET library built on top of the [Titanium Web Proxy library](https://github.com/justcoding121/Titanium-Web-Proxy). I uploaded the library to NuGet and it got over 3500 total downloads.

Here's a sample code of how to use the SeleniumProxyAuth library:

```csharp
public void Test()
{
    // Create a local proxy server
    var proxyServer = new SeleniumProxyServer();

    // Don't await, have multiple drivers at once using local proxy servers
    TestSeleniumProxyServer(proxyServer, new ProxyAuth("123.123.123.123", 80, "prox-username1", "proxy-password1"));
    TestSeleniumProxyServer(proxyServer, new ProxyAuth("11.22.33.44", 80, "prox-username2", "proxy-password2"));
    TestSeleniumProxyServer(proxyServer, new ProxyAuth("111.222.222.111", 80, "prox-username3", "proxy-password3"));

    while (true) { }
}

private async Task TestSeleniumProxyServer(SeleniumProxyServer proxyServer, ProxyAuth auth)
{
    // Add a new local proxy server endpoint
    var localPort = proxyServer.AddEndpoint(auth);

    ChromeOptions options = new ChromeOptions();
    //options.AddArguments("headless");

    // Configure Chrome driver's proxy server to the local endpoint port
    options.AddArgument($"--proxy-server=127.0.0.1:{localPort}");

    // Optional
    var service = ChromeDriverService.CreateDefaultService();
    service.HideCommandPromptWindow = true;

    // Create the driver
    var driver = new ChromeDriver(service, options);

    // Test if the driver is working correctly
    driver.Navigate().GoToUrl("https://www.myip.com/");
    await Task.Delay(5000);
    driver.Navigate().GoToUrl("https://amibehindaproxy.com/");
    await Task.Delay(5000);

    // Dispose the driver
    driver.Dispose();
}
```

## Random User Agent

For generating random user agent strings, TBS downloads a list of recent user agents from the whatismybrowser.com website. The program then randomly selects a user agent from the list and uses it in the Chrome browser
based on the popularity. This way, the program simulates different users using the browser.

## Chrome browser Extensions

In order to avoid detection, TBS injects the following Chrome browser extensions into the browser:

- [AudioContext Fingerprint Defender](https://chromewebstore.google.com/detail/audiocontext-fingerprint/pcbjiidheaempljdefbdplebgdgpjcbe?hl=en) - this extension defends against AudioContext fingerprinting by reporting fake value (small noise).
- [Canvas Fingerprint Defender](https://chromewebstore.google.com/detail/lanfdkkpgfjfdikkncbnojekcppdebfp?hl=en) - this extension defends against Canvas fingerprinting by adding fake noise to the canvas. Canvas fingerprinting is a technique used by websites to identify and track users. It involves drawing a hidden image or text on a webpage’s canvas element and then reading the pixel data back. Since the rendering of the canvas can vary slightly based on the user’s device, browser, and graphics hardware, the resulting fingerprint is unique to each user.
- [WebRTC Leak Prevent](https://chromewebstore.google.com/detail/webrtc-leak-prevent/eiadekoaikejlgdbkbdfeijglgfdalml) - this extension prevents WebRTC leaks by disabling non-proxied UDP (User Datagram Protocol) requests. WebRTC (Web Real-Time Communication) is a collection of communication protocols and APIs that enable real-time communication over peer-to-peer connections. WebRTC needs to discover your public IP address and local IP address to establish direct connections. This can lead to IP address leaks, which can compromise your privacy and security.
- [Font Finger Print Defender](https://chromewebstore.google.com/detail/font-fingerprint-defender/fhkphphbadjkepgfljndicmgdlndmoke) - this extension defends against font fingerprinting. Websites can use JavaScript to enumerate all installed fonts on a user’s system and create a unique fingerprint based on the list of fonts. By reporting a fake list of fonts, this extension helps protect user privacy and anonymity.
- [Spoof Timezone](https://chromewebstore.google.com/detail/spoof-timezone/kcabmhnajflfolhelachlflngdbfhboe) - this extension changes the timezone reported by the browser to a random timezone. Timezone information can be used to track users and identify their real location.
- [WebGL Fingerprint Defender](https://chromewebstore.google.com/detail/webgl-fingerprint-defende/olnbjpaejebpnokblkepbphhembdicik) - this extension defends against WebGL fingerprinting by reporting a fake WebGL fingerprint. WebGL (Web Graphics Library) is a JavaScript API for rendering interactive 2D and 3D graphics within any compatible web browser without the use of plug-ins. WebGL fingerprinting involves collecting information about the user’s graphics hardware and drivers to create a unique fingerprint.


## Chrome arguments

To avoid detection, TBS adds the `disable-blink-features=AutomationControlled` argument to the Chrome browser - this flag in Chrome is used to disable certain web features that indicate a browser is being controlled by an automation tool, like Selenium or Puppeteer. This can help make the automated browser appear more like a regular user-driven browser, potentially avoiding detection by websites looking for automation.

### undetected-chromedriver

[undetected-chromedriver](https://github.com/ultrafunkamsterdam/undetected-chromedriver) is a Python library that helps you to avoid detection while using Selenium with Chrome. It is a drop-in replacement for the Chrome driver that comes with Selenium. Because TravianBotSharp was developed in C#, I couldn't use this library directly, but I used some of the same techniques (Chrome flags, ) to avoid detection.

## Potential improvements

While a lot of effort went into making TBS as human-like as possible, there are still some areas where it could be improved to avoid detection.

### Keyboard input

Currently, TBS does not simulate keyboard input. It simply uses the Selenium `SendKeys` method to input text into text fields. To avoid detection, TBS could simulate keyboard input by sending keydown and keyup events with random intervals between them. It would be straightforward to implement this feature and wouldn't require a lot of changes to the existing code.

### Random mouse movements

Some websites use JavaScript to detect mouse movements and clicks to determine if a user is a bot. To avoid detection, TBS could simulate random mouse movements and clicks. It should use irregular patterns (like humans do), not have speed variations, and use different path curvatures. This could be implemented using random intervals, along with bezier curves and some random noise functions to simulate human-like mouse movements. This feature would require more effort to implement but could significantly improve the bot's stealthiness.

#### Natural mouse movements

While searching for solutions on this topic, I found [Natural mouse movements Github repository](https://github.com/DaiCapra/Natural-Mouse-Movements-Neural-Network) which uses a neural network to generate natural mouse movements. This could be a great addition to TBS to make it even more human-like. For this project such a solution is likely an overkill, but it's interesting to see how it could be implemented.

Here are a few samples of generated mouse paths, the outputs from the neural network:

![](./images/natural-mouse-movement.png)

Model input is the target coordinate (2 floats) on the screen and the output is a list of 100 coordinates, which forms the mouse path. You could use all of the coordinates, or skip some of them to make the path more human-like.

![](./images/nn.jpg)

I used [Netron App](https://netron.app/) for visualizing the neural network model architecture.

### Next Page: [TravianBotSharp Architecture](architecture.md)