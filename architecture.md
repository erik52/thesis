# TravianBotSharp architecture

From the top level, we have TbsCore (cross-platform .NET Standard library) which is used by the TbsGUI (Windows Forms application). This architecture allows us to separate the core logic from the user interface, which makes the program more modular and easier to maintain. It also allows future web-development - one could create a web interface and a backend server which would utilize the TbsCore library.

![](./images/architecture.png)

TbsCore is where the main logic of the program resides. It contains the following main classes:

![](./images/tbscore.png)

By functionality, we have data classes, service classes, and database classes.

## TbsGui

TBS Gui is a Windows Forms application that uses the TbsCore library to automate processes in the Travian game. The GUI allows users to configure the bot, set up tasks, and monitor the bot's progress. I have used Windows Forms because it's easy to use and it's a good choice for small to medium-sized applications. The GUI is simple and user-friendly, and it allows users to easily set up the bot.

![](./images/villages-build.png)

## Task Executor

`TaskExecutor` is one of the main classes in the TbsCore library. It is responsible for executing tasks in the game. This class will select the next task to execute based on the priority of the task and the start time of the task. The program will then execute the task, and potentially reschedule the task for execution at a later time. Here's a flow diagram of the `TaskExecutor` class:

![TBS working](./images/tbs.drawio.png)

Only one task can be executed at a time, because we only have one Chrome browser instance.

### Tasks

Tasks are predefined processes that the program can execute. All tasks have a defined villages where they will be carried out, the start time of the task, the priority of the task, and the current state of the task (whether the task is already being executed). Here are some examples (out of 70) tasks that the program can execute:

- Building construction
- Sending and training troops
- Sending resources
- Founding new villages
- Sending the hero on adventures
- Scouting enemy villages, etc.

Tasks are stored in a task list, and are scheduled based on user settings. They extend the `BotTask` base class, which contains the following properties:

- `Village` - the village where the task will be carried out
- `Priority` - the priority of the task. Some tasks, like attacking and defending a village are time-critical and have a high priority, while other tasks, like building construction, are not time-critical and have a lower priority.
- `ExecuteAt` - the start time of the task
- Abstract `Execute()` method - the method that will be called when the task executor executes the task. This method is `async`, which helps with the asynchronous nature of the program.
- `RetryCounter` - in case of an error, the task will be retried a certain number of times
- `TaskStage` - the current state of the task, which lets TaskExecutor know whether any of the tasks is already being executed.

## Parsers

Parsers are used to extract important information from the HTML code of the Travian game. The program uses the [HtmlAgilityPack](https://html-agility-pack.net/) library to parse the HTML code and extract the necessary information. The program then uses this information to make decisions and perform tasks.

HtmlAgilityPack is a .NET library that aids users with parsing HTML code. It allows you to easily extract data from HTML code using XPath or LINQ queries. Because the TravianBotSharp supported both Travian and TTWars servers, parsers often had to implement parsers for both servers, as they often have different HTML structures.

### LINQ queries

LINQ stands for Language Integrated Query, and it is a feature of C# that allows you to query data from different data sources.
In the case of HtmlAgilityPack, you can use LINQ queries to extract data from HTML code. Here's an example of a LINQ query that extracts all the links from an HTML document:

```csharp
var msgs = htmlDoc.GetElementbyId("n6");
var container = msgs.Descendants("div").FirstOrDefault(x => x.HasClass("speechBubbleContainer"));
if (container == null) return 0;
var msgCount = container.Descendants("div").FirstOrDefault(x => x.HasClass("speechBubbleContent")).InnerHtml;

```

LINQ is a bit more user-friendly than XPath, and it's easier to read and write. The same function in XPath would look like this:

```csharp
var speechBubbleContent = htmlDoc.DocumentNode.SelectSingleNode("//div[@id='n6']//div[contains(@class, 'speechBubbleContainer')]//div[contains(@class, 'speechBubbleContent')]");
if (speechBubbleContent != null) return speechBubbleContent.InnerText;
```

## Database

The program uses a SQLite database to store information about the account information and its settings. The program reads this information from the database at the startup, and then writes the information back to the database when the program is closed.

The database is accessed using the Entity Framework Core library, which is an Object-Relational Mapping (ORM) library for .NET. It helps with the communication between the program and the database, and allows you to work with the database using C# objects.

In initial versions of the TBS I have used a simple text (.txt) file to save the JSON-serialized Account classes. This was later replaced with SQLite database, as it's more efficient and less error-prone.

### SQLite

SQLite is a lightweight, self-contained, and transactional SQL database engine. It is the most widely deployed database engine in the world. SQLite is built into all mobile phones and most computers and comes bundled inside countless other applications that people use every day.

I have used it because TBS is a single-user application, and it doesn't require a full-fledged database server like MySQL or SQL Server.

## TBS Functionality

I won't go into details about the functionality of the program, as it's quite extensive. Here are some of the main features of the program:

### Wave Builder

When attacking with catapults in Travian, it's often beneficial to send multiple waves of attacks to destroy the enemy's buildings. When playing manually, this can be quite tedious, and users usually used Firefox to do so: they would open multiple tabs with the same attack, enabled Offline Mode, click `Send Attack` on all tabs, disable Offline Mode, and very quickly click `Resend` on all tabs. This would lead to multiple waves of attacks being sent in the same second, so there was no way for the enemy to send defense between the waves.

#### Wave Builder scripts

Many browser extensions were developed to help with building waves, most notable one being [TravianWaveBuilder](https://github.com/adipiciu/Travian-scripts). It's a Greasemonkey script that injects custom JavaScript into the Travian game, allowing users to easily send multiple waves of attacks. Because it's open-source, I was able to use the code to create a similar feature in the TravianBotSharp program.

#### Wave Builder in TravianBotSharp

The feature is called `Wave Builder` and allows users to easily create multiple waves of attacks and send them in a short time frame. I used direct POST requests to the server to send the attacks, as doing so with Chrome browser and waiting for each attack to load isn't feasible. TBS uses [RestSharp](https://restsharp.dev/) library, an HTTP API client for .NET. This library allows TBS to send POST requests, and also supports cookies and proxy authentication, which is important for sending attacks from multiple accounts (to avoid getting detected).


With Wave Builder feature, users can send multiple waves of attacks within a second, and it allows sub-second precision even when using Proxy (which adds to the latency overhead). This is achieved by calculating the roundtrip of a ping request (using .NET's [Ping Class](https://learn.microsoft.com/en-us/dotnet/api/system.net.networkinformation.ping)) to the Travian server and then sending the attacks at the calculated time.

![wave builder feature](./images/wave_builder_feature.png)

Besides sending real attacks, the Wave Builder feature also supports sending fake attacks. Fake attacks are attacks that are sent to the enemy, but there are minimal troops - usually you'd send 19 troops + a catapult. Because enemy can calculate the slowest unit of the attack (by calculating the time it takes for the attack to arrive), fake attacks contain 1 catapult, which is the slowest unit in the game.

#### Wave Builder for defense

The Wave Builder feature can also be used for defense, and it will try to send small amounts of defensive units each second during the multi-second attack. This way, we can defend against some waves of catapults, which  leads to the enemy not being able to destroy our buildings.

![defense between seconds](./images/same_sec_deff.webp)


### Combat Simulator

The Combat Simulator is a feature that simulates battles between two players. It calculates the outcome of the battle based on the number of attacking and defending troops, along with other parameters (wall level, population, palace level, metalurgy level, hero and hero items, etc.).

The most popular Travian combat simulator is [Travian WarSim2](http://travian.kirilloid.ru/warsim2.php), developed by [kirilloid](https://github.com/kirilloid). Kirill open-sourced the code of the simulator to Github ([kirilloid/travian](https://github.com/kirilloid/travian)), so I was able to use the code to integrate a similar feature in the TravianBotSharp program. This allows the bot to take actions based on the outcome of the simulated battle. Example: the bot can scout the enemy village, check the report of the scout, and then decide whether to attack the village based on the number of casualties. In some cases you can raid more resources than you lose troops, so it's beneficial to attack even if you lose some troops.

![](./images/warsim.jpeg)

### Inactive player search

The Inactive player search feature allows users to search for inactive players in the Travian game. Inactive players are players who haven't logged in for a certain amount of time . These players are usually easy targets for attacks, and they can be a good source of resources. The feature uses the [inactivesearch.it](https://www.inactivesearch.it/inactives/) website to search for inactive players. Because the website doesn't have an API, the feature uses a web scraping technique to get the data. The feature then adds the inactive players to the list of farm targets, and the bot can then attack them.

#### Map.sql

Travian server allows you to download a database of all villages, their coordinates, owners, and population. You can download the `map.sql` by adding the `map.sql` in the URL of your game world, eg. `https://ts5.x1.europe.travian.com/map.sql`. It gets updated at midnight each day, so tools like InactiveSearch can download the `map.sql` file each day and then compares the data from previous days to find inactive players.

```sql
INSERT INTO `x_world` VALUES (273,72,200,3,25825,'Ker',496,'Ker',26,'3OK',316,NULL,FALSE,NULL,NULL,NULL);
INSERT INTO `x_world` VALUES (1005,2,198,2,23835,'Мълчаливият Дявол',1793,'xaxaxa',140,'EMP3',388,NULL,TRUE,NULL,NULL,NULL);
INSERT INTO `x_world` VALUES (1406,2,197,2,27506,'Справедливият Дявол',1793,'xaxaxa',140,'EMP3',517,NULL,FALSE,NULL,NULL,NULL);
INSERT INTO `x_world` VALUES (1408,4,197,2,29477,'Спокойният Дявол',1793,'xaxaxa',140,'EMP3',190,NULL,FALSE,NULL,NULL,NULL);
INSERT INTO `x_world` VALUES (1440,36,197,3,27669,'Tiksi',516,'Klonkku',91,'EMP2',143,NULL,FALSE,NULL,NULL,NULL);
INSERT INTO `x_world` VALUES (1577,173,197,2,23455,'01- Nadirşah',2845,'kefkef',26,'3OK',419,NULL,TRUE,NULL,NULL,NULL);
...
```

Here's what the values in the `map.sql` file represent:

1. **ID** - unique identifier of that field
2. **X** - x coordinate of the village
3. **Y** - y coordinate of the village
4. **Tribe** - tribe of the village (1 = Roman, 2 - Teuton, 3 - Gaul, 4 - Nature, 5 - Natar, 6 - Egyptian, 7 - Huns, 8 - Spartans)
5. **Village ID** - unique identifier of the village
6. **Village Name** - name of the village
7. **Player ID** - unique identifier of the player
8. **Player Name** - name of the player
9. **Alliance ID** - unique identifier of the alliance
10. **Alliance Name** - name of the alliance
11. **Population** - population of the village - this is used to determine if the player is inactive
12. **Region** - In which region of the Travian world this village belong to [If the server is Annual Special]
13. **Capital** - Whether this village is the capital village of the player
14. **City** - Whether this village is a city [If the server is Annual Special]
15. **Harbor** - Whether this village is a harbor [If the server is Annual Special]
16. **Victory points** - Victory points of the village [If the server is Annual Special]

### Next Page: [Competition, monetization, and potential improvements](monetization-and-improvements.md)