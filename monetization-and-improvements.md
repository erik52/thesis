# Competition, monetization, and potential improvements

## Competition

Before diving into the potential monetization approaches I could take, it's important to understand the competitive landscape. Here are some of the main competitors in the Travian bot space:

- [Travian Builder](https://apkcombo.com/travian-builder/com.wxuier.tbot/) was a popular bot that was available on both Android and Windows. It was free to use and only relied on donations. The bot was discontinued in 2019, but it was widely used before that.
- [TravianTactics](https://traviantactics.com/) uses a freemium model, offering additional paid features. It a web browser extension that runs JavaScript code on the Travian website. They also accept donations. This is a popular bot that has been around for a long time.
- [TraviBot](https://travibot.com/en/) offers paid program with a free trial. It's a desktop application. They only support Windows OS, so it was likely built with .NET (the same as TravianBotSharp). They charge between $3 and $5 per month, or $10 to $20 for a yearly license.
- [TCommanderBot](https://www.tcommanderbot.com/) - similar to TraviBot, it's also an Windows desktop application. It supports many features that TBS does as well. They offer a free trial for a week, and then charge $2 per month per game account.

Overall, the market for Travian bots is quite competitive, and prices are relatively low.

## Monetization

While I have not monetized the TravianBotSharp program, I have considered several ways to do so. Here are some ideas for monetizing the program:

1. **Donations**. Vinaghost (the current maintainer of the project) has added a donation button to the program, allowing users to donate to the project. This is a simple and effective way to support the project financially. Looking at his [ko-fi page](https://ko-fi.com/vinaghost), he has received over $800 in donations over the years, which gets your far in Vietnam.
2. **Hosted version**. Another way to monetize the program is to offer a hosted version of the program, so the program runs on your server instead of the user's computer. The current version of the program would need to be refactored to support hosted version, as currently, it's a desktop application. Many other Travian bots have taken this approach.
3. **Premium features**. Vinaghost has also added a premium feature to the program, which allows users to access additional features for a fee. This is a common way to monetize software, as it allows users to pay for the features they want.

## Potential Improvements

While the program is quite mature and feature-rich, there are still several potential improvements that could be made:

1. Some additional bot detection prevention features, as outlined in the [Avoiding Detection](avoiding-detection.md) section
2. Web app for managing the bot - currently, it's a Windows Desktop application, with web app you could manage the bot from any device, and host the bot client on a server (cheap VPS)
3. Offering (paid) hosted solutions - so users don't have to run the bot on their own computer.
4. TBS Server for cross-account collaboration - so bot clients could send resources and troops from one account to another

### Web app for managing the bot

The TbsCore (main library of the program) could be integrated into a ASP.NET Core backend web application. We'd need to expose all bot settings through an API (eg. REST API), and then create a frontend web application that communicates with the backend. This would allow users to manage the bot from anywhere and from any smart device, and we could host the bot client on a server (eg. cheap VPS).

![](./images/web-app.jpeg)

### Hosted TBS Client

After refactoring the program use a Web App for the GUI, it would be possible to host the bot client (ASP.NET backend) on a VPS. This would allow users to run the bot without having to keep their computer on all the time. This would be a paid service due to server costs.

### TBS Server

Another great addition would be to connect TBS clients to a TBS Server. This would allow users to automatically collaborate between accounts. Functionality would include:

- Sending resources between accounts
- Sending defensive troops between accounts, in case of an attack
- Sending multiple attacks from multiple accounts to a specific enemy village at the same time

In the late game, most of the time spent on Travian is planning offensive and defensive actions, as it requires a lot of coordination between multiple people. This feature would be a game-changer for Travian players, and would be a premium feature.

![](./images/tbs-server.jpeg)

### Next Page: [Community](community.md)